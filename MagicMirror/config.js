/* Magic Mirror Config Sample
 *
 * By Michael Teeuw http://michaelteeuw.nl
 * MIT Licensed.
 *
 * For more information how you can configurate this file
 * See https://github.com/MichMich/MagicMirror#configuration
 *
 */

var config = {
	address: "localhost", // Address to listen on, can be:
	                      // - "localhost", "127.0.0.1", "::1" to listen on loopback interface
	                      // - another specific IPv4/6 to listen on a specific interface
	                      // - "", "0.0.0.0", "::" to listen on any interface
	                      // Default, when address config is left out, is "localhost"
	port: 8080,
	ipWhitelist: ["127.0.0.1", "::ffff:127.0.0.1", "::1"], // Set [] to allow all IP addresses
	                                                       // or add a specific IPv4 of 192.168.1.5 :
	                                                       // ["127.0.0.1", "::ffff:127.0.0.1", "::1", "::ffff:192.168.1.5"],
	                                                       // or IPv4 range of 192.168.3.0 --> 192.168.3.15 use CIDR format :
	                                                       // ["127.0.0.1", "::ffff:127.0.0.1", "::1", "::ffff:192.168.3.0/28"],

	language: "en",
	timeFormat: 24,
	units: "metric",

	modules: [
		{
			module: "alert",
		},
		// Logo
		{
			module: "MMM-ImagesPhotos",
			position: "center_top",
			config: {
				opacity: 0.9,
				animationSpeed: 500,
				updateInterval: 5000,
			}
		},
		{
			module: 'calendar_monthly',
			position: 'top_left',
			config: {
				// The config property is optional
				// Without a config, a default month view is shown
				// Please see the 'Configuration Options' section for more information
				}
		},
	
		{
			module: "clock",
			position: "top_left",	// This can be any of the regions.
			config: {
				// The config property is optional.
	 			// See 'Configuration options' for more information.
	 			}
		},
		
		{
			module: "MMM-Counter",
			position: "top_left",	
			config: {

	 			}
		},
 	 
		{
			module: "updatenotification",
			position: "top_bar"
		},
         /*
	  *	{
	  *		module: "calendar",
	  *		header: "Environmental events",
	  *		position: "top_left",
	  *		config: {
	  *			calendars: [
	  *				{
	  *					symbol: "calendar-check-o ",
	  *					url: "https://outlook.office365.com/owa/calendar/c2ab49a792ac41d49bf1e161b70ef353@clovertech.eu/619f8e0f33a04aaeb16b161c8ce7d89115443390252331776684/calendar.ics"
	  *				}
	  *			]
	  *		}
	  *	},
          */

	 	{
  	 		module: 'worldclock',
  	 		position: 'top_left', // This can be any of the regions, best results in top_left or top_right regions
  			config: {
    	 		// See 'Configuration options' for more information.
         
    	 		timeFormat: 'HH:mm A', //defined in moment.js format()
    	 		style: 'top', //predefined 4 styles; 'top', 'left','right','bottom'
    	 		clocks: [
	 						{
	 							title: "Petrovaradin",
	 							timezone: "GMT +01:00)", //When omitted, Localtime will be displayed. It might be not your purporse, I bet.
	 							
	 						},
							{
	 							title: "Hoffman Estates", // Too long title could cause ugly text align.
	 							timezone: "America/Chicago", //When omitted, Localtime will be displayed. It might be not your purporse, I bet.
	 							
	 						},
    	 	            ]
  	 		}
	 	},
		{
			module: "compliments",
			position: "lower_third"
		},
	
		{
			module: "weatherforecast",
			position: "top_right",
			header: "Weather Forecast",
			config: {
				location: "Petrovaradin, RS",
				locationID: "3193406",  //ID from http://www.openweathermap.org/help/city_list.txt
				appid: "ccdb3f902e5d7ce6674b3980712215f7"
			}
		},

		
	/*{
	/	module: "MMM-iHaveBeenThere",
	/	position: "bottom_bar",	// bigger place is recommended.
	/	config: {
			// The config property is optional.
			// If no config is set, an example MMM-iHaveBeenThere is shown.
			// See 'Configuration options' for more information.
	/	}
	/},
	*/


	{
	        module: "MMM-Buller",
	        position: "lower_third",
	        header: "Buller",
	        config: {

			debug: false,
        			lists: [
			{
				type: 'gTasks',
              			name: 'MMM',   
              			label: 'da4throux tasks',
              			updateInterval: 1000 * 60 * 1 * 1, //every minutes
              			icon: 'fas fa-male',
            		},
            		{
              			type: 'gTasks',
              			name: 'Compliments',   
              			label: 'da4throux compliments',
              			color: 'pink',
              			icon: 'fas fa-female',
              			updateInterval: 1000 * 60 * 1 * 1, //every minutes
            		}
        				],
   
	        }
    			},


		// QR code
    		{
       		module: "MMM-SimpleLogo",
       			position: "bottom_bar",    // This can be any of the regions.
        		config: {
            // The config property is optional.
            // See 'Configuration options' for more information.
       		}
		
		}
	


       /*
	*      {
	*		module: "newsfeed",
	*		position: "bottom_bar",
	*		config: {
	*			feeds: [
	*				{
	*					title: "New York Times",
	*					url: "http://www.nytimes.com/services/xml/rss/nyt/HomePage.xml"
	*				}
	*			],
	*			showSourceTitle: true,
	*			showPublishDate: true
	*		}
	*	},
	*/
	]

};





/*************** DO NOT EDIT THE LINE BELOW ***************/
if (typeof module !== "undefined") {module.exports = config;}
