Module.register("MMM-Image2Display",{
	// Default module config.
	defaults: {
		img: { 
			url: 'modules/MMM-Image2Display/public/qr-code.png', 
		}
	},
	
	
	getStyles: function() {
		return [
			'modules/MMM-Image2Display/css/MMM-Image2Display.css'
		]
	}, 
	
	getTemplate: function() {
		return "MMM-Image2Display.njk";
	}, 

	getTemplateData: function() {
		return this.config;
	},
	
	start: function() {
		var self = this;
		
		self.updateDom(); 
		
	},
	

	
});



	


