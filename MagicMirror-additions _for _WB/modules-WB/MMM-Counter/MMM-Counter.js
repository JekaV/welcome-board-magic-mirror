Module.register("MMM-Counter",{
	// Default module config.
	defaults: {
		cartridges: { 
			img: 'modules/MMM-Counter/public/printer.png', 
			value: 0,
			valueTo: 361389458,
			incrementValBy: 6,
			output: '0'
		},
		phones: {
			img: 'modules/MMM-Counter/public/phone.png', 
			value: 0,
			valueTo: 12991028,
			incrementValBy: 2,
			output: '0'
		},
		lbs: {
			img: 'modules/MMM-Counter/public/lbs.png',
			value: 0,
			valueTo: 348200000,
			incrementValBy: 5,
			output: '0'
		}
	},
	
	getScripts: function() {
		return [
			'modules/MMM-Counter/vendor/jquery-2.2.3.min.js', 
		]
	},
	
	getStyles: function() {
		return [
			'modules/MMM-Counter/css/MMM-Counter.css'
		]
	}, 
	
	getTemplate: function() {
		return "MMM-Counter.njk";
	}, 

	getTemplateData: function() {
		return this.config;
	},
	
	prepereOutput: function(val) {
		return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	},
	
	calcCountData: function(obj) {
		self.config.obj.value += self.config.obj.incrementValBy;
		self.config.obj.output = self.prepereOutput(self.config.obj.value);	
		self.updateDom(); 
	},
	
	start: function() {
		var self = this;
		
		self.config.cartridges.value = self.config.cartridges.valueTo;
		self.config.phones.value = self.config.phones.valueTo;
		self.config.lbs.value = self.config.lbs.valueTo;
		
		setInterval(function() {

			self.config.cartridges.value += self.config.cartridges.incrementValBy;
			self.config.cartridges.output = self.prepereOutput(self.config.cartridges.value);	
			
			self.updateDom(); 
		
		}, 3000); 
		
		setInterval(function() {
			
			self.config.phones.value += self.config.phones.incrementValBy;
			self.config.phones.output = self.prepereOutput(self.config.phones.value);	
			
			self.updateDom(); 
		
		}, 1800); 
		
		setInterval(function() {
			
			self.config.lbs.value += self.config.lbs.incrementValBy;
			self.config.lbs.output = self.prepereOutput(self.config.lbs.value);	
			
			self.updateDom(); 
		
		}, 2500); 	
	},	
});



	


