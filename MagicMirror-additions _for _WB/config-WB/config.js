/* Magic Mirror Config Sample
 *
 * By Michael Teeuw http://michaelteeuw.nl
 * MIT Licensed.
 *
 * For more information how you can configurate this file
 * See https://github.com/MichMich/MagicMirror#configuration
 *
 */

var config = {
	address: "localhost", //"192.168.0.26", // Address to listen on, can be:
	// - "localhost", "127.0.0.1", "::1" to listen on loopback interface
	// - another specific IPv4/6 to listen on a specific interface
	// - "", "0.0.0.0", "::" to listen on any interface
	// Default, when address config is left out, is "localhost"
	port: 8080,
	ipWhitelist: ["127.0.0.1", "192.168.0.21", "192.168.0.26", "::ffff:127.0.0.1", "::1"], // Set [] to allow all IP addresses
	// or add a specific IPv4 of 192.168.1.5 :
	// ["127.0.0.1", "::ffff:127.0.0.1", "::1", "::ffff:192.168.1.5"],
	// or IPv4 range of 192.168.3.0 --> 192.168.3.15 use CIDR format :
	// ["127.0.0.1", "::ffff:127.0.0.1", "::1", "::ffff:192.168.3.0/28"],

	language: "en",
	timeFormat: 24,
	units: "metric",

	modules: [
		{
			module: "alert",
			//default module
		},
		//-----------------------------------------------------------------------------
		// LOGO
		{
			module: "MMM-ImagesPhotos",
			position: "center_top",
			config: {
				opacity: 0.9,
				animationSpeed: 500,
				updateInterval: 5000,
				//module url: https://github.com/roramirez/MMM-ImagesPhotos.git
			}
		},
		//------------------------------------------------------------------------------------	
		{
			module: 'MMM-Weather-Now',
			position: 'top_left',
			config: {
				api_key: 'b00ed46eb1214bf5961226266e620d13',
				lat: 45.252640,
				lon: 19.873249,
				units: 'M',
				lang: 'en',
				interval: 900000,
				tableView: true,
				//module url: https://github.com/roramirez/MMM-ImagesPhotos.git
			}
		},
		//--------------------------------------------------------------------------------------
		{
			module: "clock",
			position: "top_left",	// This can be any of the regions.
			config: {
				// The config property is optional.
				// See 'Configuration options' for more information.
				//default module
			}
		},
		//-----------------------------------------------------------------------------------------		
		{
			module: 'calendar_monthly',
			position: 'top_left',
			config: {
				// The config property is optional
				// Without a config, a default month view is shown
				// Please see the 'Configuration Options' section for more information
				//module url: https://github.com/KirAsh4/calendar_monthly
			}
		},
		//--------------------------------------------------------------------------------------
		{
			module: "updatenotification",
			position: "top_bar"
			//default module
		},
		//--------------------------------------------------------------------------------------
		{
			module: "compliments",
			position: "lower_third"
			//default module
		},

		//-------------------------------------------------------------------------------------
		//CUSTOM TEXT
		{
			module: "MMM-RemoteCompliments",
			position: "lower_third",
			//header: "GOOGLE DRIVE DATA",
			config: {
				// module url: https://github.com/mitchelltmarino/MMM-RemoteCompliments.git
						/*
		 				* Data loaded from the GOOGLE DRIVE
					  * Related to: distribucija2018@gmail.com
						* 
		        * */
			}
		},
		//--------------------------------------------------------------------------------------
		//BIG MAP
		{
			module: "MMM-SimpleLogo",
			position: "bottom_bar",    // This can be any of the regions.
			config: {
				//module url: https://github.com/frdteknikelektro/MMM-SimpleLogo.git
			}
		},
		//--------------------------------------------------------------------------------------
		//COUNTER
		// based on "impact counter" from http://www.clovertech.com/#responsibility
		{
			module: "MMM-Counter",
			position: "bottom_bar",
			config: {
				//module url:https://gitlab.com/JekaV/welcome-board-magic-mirror/tree/master/MagicMirror-additions_for_WB/modules-WB/MMM-Counter
		},
		//--------------------------------------------------------------------------------------
		// QR CODES
		{
			module: "MMM-Image2Display",
			position: "top_right",
			config: {
				//module url: https://gitlab.com/JekaV/welcome-board-magic-mirror/tree/master/MagicMirror-additions_for_WB/modules-WB/MMM-Image2Display
			}
		}

	]
};

/*************** DO NOT EDIT THE LINE BELOW ***************/
if (typeof module !== "undefined") { module.exports = config; }
