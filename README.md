# Welcome board - Magic Mirror

Welcome Board - Magic Mirror is used at the company's reception to welcome visitors with a personal greeting on a screen.
Visitors can instantly see an up-to-date overview of all the important information about their appointment.
The "Greetings part" on the Magic Mirror is edited from the Google sheet and the written text is adjusted to the screen size.
In addition to the "Greetings part", there are also displayed information about the company, news, clock, and weather information.

Required:

1. https://magicmirror.builders/ -  The open source modular smart mirror platform.
2. Raspberry Pi 3B + 16gb Micro SD card +  Micro USB power supply (2.1 A)
3. Two-way mirror
4. 32" TV + HDMI cable
5. Chipboard frame